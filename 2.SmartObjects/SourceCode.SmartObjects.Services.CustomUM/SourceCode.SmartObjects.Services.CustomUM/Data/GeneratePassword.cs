﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Security.Cryptography;

using SourceCode.SmartObjects.Services.ServiceSDK.Objects;
using Attributes = SourceCode.SmartObjects.Services.ServiceSDK.Attributes;
using SourceCode.SmartObjects.Services.ServiceSDK.Types;

namespace SourceCode.SmartObjects.Services.CustomUM.Data
{
    /// <summary>
    /// Sample implementation of a Static Service Object.
    /// The class is decorated with a ServiceObject Attribute providing definition information for the Service Object.
    /// The Properties and Methods in the class are each decorated with attributes that describe them in Service Object terms
    /// This sample implementation contains two Properties (Number and Text) and two Methods (Read and List)
    /// </summary>
    [Attributes.ServiceObject("GeneratePassword", "Generate Password", "K2 CustomUM service to generate passwords")]
    class GeneratePassword
    {
	
		/// <summary>
        /// This property is required if you want to get the service instance configuration 
        /// settings in this class
        /// </summary>
        private ServiceConfiguration _serviceConfig;
        public ServiceConfiguration ServiceConfiguration
        {
            get { return _serviceConfig; }
            set { _serviceConfig = value; }
        }
		
        #region Class Level Fields

        #region Private Fields
        private string _password = string.Empty;
        #endregion

        #endregion

        #region Properties with Property Attribute

        #region string Password
        [Attributes.Property("Password", SoType.Text, "Password", "Random password result")]
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
        #endregion

        #endregion

        #region Default Constructor
        /// <summary>
        /// Instantiates a new ServiceObject1.
        /// </summary>
        public GeneratePassword()
        {
            // No implementation necessary.
        }
        #endregion

        #region Methods with Method Attribute

        #region GeneratePassword Generate(Length, ToLowerCase)
        [Attributes.Method("Generate", MethodType.Execute, "Generate", "Generate random password",
            new string[] { }, 
            new string[] { }, 
            new string[] { "Password" })] 
        public GeneratePassword Generate(
            [Attributes.Parameter(  //decorate the method's input parameter so that K2 can discover it
                "Length", // Parameter Name
                SoType.Number, // Parameter Type
                "Length of password", // Parameter Display Name
                "", // Parameter Description
                true //required
            )] int length,
            [Attributes.Parameter(  
                "ToLowerCase", 
                SoType.YesNo, 
                "Convert to lower case?", 
                "",
                true //required
            )] bool toLowerCase
        )
        {            
            if (length < 8) throw new ArgumentException("Password needs to have at least 8 characters");

            //Removed O, o, 0, l, 1 to prevent confusions
            string allowedLetterChars = "abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ";
            string allowedNumberChars = "23456789";
            string allowedSpecialChars = "!@#$";
            char[] chars = new char[length];

            //generate random seed
            byte[] randomBytes = new byte[4];
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetBytes(randomBytes);
            int seed = (randomBytes[0] & 0x7f) << 24 |
                        randomBytes[1] << 16 |
                        randomBytes[2] << 8 |
                        randomBytes[3];

            //random generator
            Random rd = new Random(seed);

            // decide how many special characters
            int maxNoOfSpecialChars = rd.Next() % 3;

            int modResult = 0;
            int specialCharCount = 0;
            for (int i = 0; i < length; i++)
            {
                modResult = rd.Next() % 3;
                if (modResult == 0 || modResult == 2)
                {
                    if (modResult == 2 && specialCharCount < maxNoOfSpecialChars)
                    {
                        chars[i] = allowedSpecialChars[rd.Next(0, allowedSpecialChars.Length)];
                        specialCharCount++;
                    }
                    else
                    {
                        modResult = 0;
                    }

                    if (modResult == 0)
                    {
                        chars[i] = allowedLetterChars[rd.Next(0, allowedLetterChars.Length)];
                    }
                }
                else
                {
                    chars[i] = allowedNumberChars[rd.Next(0, allowedNumberChars.Length)];
                }
            }
            string strResult = new string(chars);
            if (toLowerCase) strResult = strResult.ToLower();
            GeneratePassword retResult = new GeneratePassword();
            retResult.Password = strResult;
            return retResult;
        }
        #endregion

        #endregion
    }
}