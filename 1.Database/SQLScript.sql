USE [K2]
GO

/****** Object:  StoredProcedure [CustomUM].[UpdateUser]    Script Date: 17/6/2015 5:56:46 PM ******/
DROP PROCEDURE [CustomUM].[UpdateUser]
GO

/****** Object:  StoredProcedure [CustomUM].[GetUserGroups]    Script Date: 17/6/2015 5:56:46 PM ******/
DROP PROCEDURE [CustomUM].[GetUserGroups]
GO

/****** Object:  StoredProcedure [CustomUM].[GetGroupUsers]    Script Date: 17/6/2015 5:56:46 PM ******/
DROP PROCEDURE [CustomUM].[GetGroupUsers]
GO

/****** Object:  StoredProcedure [CustomUM].[DeleteUserAndUserGroup]    Script Date: 17/6/2015 5:56:46 PM ******/
DROP PROCEDURE [CustomUM].[DeleteUserAndUserGroup]
GO

/****** Object:  StoredProcedure [CustomUM].[DeleteGroupAndUserGroup]    Script Date: 17/6/2015 5:56:46 PM ******/
DROP PROCEDURE [CustomUM].[DeleteGroupAndUserGroup]
GO

/****** Object:  StoredProcedure [CustomUM].[CreateGroupUser]    Script Date: 17/6/2015 5:56:46 PM ******/
DROP PROCEDURE [CustomUM].[CreateGroupUser]
GO

/****** Object:  StoredProcedure [CustomUM].[CreateGroupUser]    Script Date: 17/6/2015 5:56:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [CustomUM].[CreateGroupUser] 
	-- Add the parameters for the stored procedure here
	@userID int, 
	@groupID int
AS
BEGIN
	-- check if the combination existsed
	DECLARE @recordExist int
	SELECT 
		@recordExist = COUNT(*) 
	FROM
		[CustomUM].[UserGroup]
	WHERE
		[UserID] = @userID AND
		[GroupID] = @groupID

	IF @recordExist = 0
	BEGIN
		INSERT INTO [CustomUM].[UserGroup](GroupID, UserID)
			VALUES(@groupID, @userID)
	END
END


GO

/****** Object:  StoredProcedure [CustomUM].[DeleteGroupAndUserGroup]    Script Date: 17/6/2015 5:56:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [CustomUM].[DeleteGroupAndUserGroup] 
	@groupID int
AS
BEGIN
	BEGIN TRAN T1
	DELETE FROM [CustomUM].[UserGroup] WHERE [GroupID] = @groupID
	DELETE FROM [CustomUM].[Group] WHERE [GroupID] = @groupID
	COMMIT TRAN T1
END

GO

/****** Object:  StoredProcedure [CustomUM].[DeleteUserAndUserGroup]    Script Date: 17/6/2015 5:56:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [CustomUM].[DeleteUserAndUserGroup] 
	@userID int 
AS
BEGIN
	BEGIN TRAN T1
	DELETE FROM [CustomUM].[UserGroup] WHERE [UserID] = @userID
	UPDATE [CustomUM].[User] SET [ManagerID] = NULL WHERE [ManagerID] = @userID
	DELETE FROM [CustomUM].[User] WHERE [UserID] = @userID
	COMMIT TRAN T1
END

GO

/****** Object:  StoredProcedure [CustomUM].[GetGroupUsers]    Script Date: 17/6/2015 5:56:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [CustomUM].[GetGroupUsers] 
	-- Add the parameters for the stored procedure here
	@GroupID int
AS
BEGIN
	SELECT 
	u.[UserID] AS UserID
	,u.[UserName] AS UserName
	,u.[UserEmail] AS UserEmail
	,u.[UserDescription] AS UserDescription
	,m.[UserID] AS UserManagerID
	,m.[UserName] AS UserManager
	FROM 
		[CustomUM].[UserGroup] ug 
		LEFT OUTER JOIN [CustomUM].[User] u ON ug.UserID = u.UserID
		LEFT OUTER JOIN [CustomUM].[Group] g ON ug.GroupID = g.GroupID
		LEFT OUTER JOIN [CustomUM].[User] m ON u.ManagerID = m.UserID
	WHERE
		ug.GroupID = @GroupID
END


GO

/****** Object:  StoredProcedure [CustomUM].[GetUserGroups]    Script Date: 17/6/2015 5:56:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [CustomUM].[GetUserGroups] 
	@UserID int
AS
BEGIN
	SELECT 
		g.[GroupID] As GroupID
		,g.[GroupName] As GroupName
		,g.[GroupDescription] AS GroupDescription
	FROM 
		[CustomUM].[UserGroup] ug 
		LEFT OUTER JOIN [CustomUM].[User] u ON ug.UserID = u.UserID
		LEFT OUTER JOIN [CustomUM].[Group] g ON ug.GroupID = g.GroupID
		LEFT OUTER JOIN [CustomUM].[User] m ON u.ManagerID = m.UserID
	WHERE
		ug.UserID = @UserID
END


GO

/****** Object:  StoredProcedure [CustomUM].[UpdateUser]    Script Date: 17/6/2015 5:56:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [CustomUM].[UpdateUser] 
	@UserID				INT 
	,@Username		NVARCHAR(200) = NULL
	,@Password		NVARCHAR(200) = NULL
	,@Description	NVARCHAR(200) = NULL
	,@Email			NVARCHAR(200) = NULL
	,@ManagerID		INT = NULL
AS
BEGIN
	DECLARE @IsAzure BIT = CASE WHEN @@VERSION LIKE N'%Azure%' THEN 1 ELSE 0 END;
	DECLARE @Encrypted NVARCHAR(MAX) = NULL;
	DECLARE @SQLMAJORVERSION TINYINT = @@MICROSOFTVERSION / 0x01000000

	-- http://msdn.microsoft.com/en-us/library/ms174415.aspx
	-- 256 bits (32 bytes) for SHA2_256, and 512 bits (64 bytes) for SHA2_512 applies to SQL Server 2012 through SQL Server 2014.
	DECLARE @PasswordHash VARBINARY(64) = NULL

	IF @Password IS NOT NULL AND LTRIM(@Password) != ''
	BEGIN
		IF(@SQLMAJORVERSION = 10) BEGIN --if SQL Server 2008
			SET @PasswordHash = HASHBYTES(N'SHA1', @Password);
			END
		ELSE BEGIN
			SET @PasswordHash = HASHBYTES(N'SHA2_512', @Password);
		END

		IF (@IsAzure = 0) BEGIN

			DECLARE @sql NVARCHAR(4000) = N'
				OPEN SYMMETRIC KEY [SCSSOKey]
					DECRYPTION BY CERTIFICATE [SCHostServerCert];
	
				SET @Encrypted = ENCRYPTBYKEY(Key_GUID(N''SCSSOKey''), @Password);

				CLOSE SYMMETRIC KEY [SCSSOKey];';

			EXEC sp_executesql @sql, N'@Password NVARCHAR(200), @Encrypted NVARCHAR(MAX) OUTPUT', @Password=@Password, @Encrypted=@Encrypted OUTPUT;

		END; --if
	END

	UPDATE [CustomUM].[User]
		SET
			[UserName] = ISNULL(@Username, [UserName])
			,[UserPassword] = ISNULL(@Encrypted, [UserPassword])
			,[PasswordHash] = ISNULL(@PasswordHash, [PasswordHash])
			,[UserDescription] = ISNULL(@Description, [UserDescription])
			,[UserEmail] = ISNULL(@Email, [UserEmail])
			,[ManagerID] = ISNULL(@ManagerID, [ManagerID])
		WHERE
			 [UserID] = @UserID

-- Update Identity Cache
	UPDATE 
		[Identity].[Identity]
	SET 
		[ExpireOn] = GETDATE()
		,[Resolved] = 0
		,[ContainersResolved] = 0
		,[ContainersExpireOn] = GETDATE()
		,[MembersResolved] = 0
		,[MembersExpireOn] = GETDATE()
	WHERE
		[Name] = (SELECT [UserName] FROM [CustomUM].[User] WHERE [UserID] = @UserID)
END

GO


